<?php
namespace Segaja;

class Request
{
    public const INT            = 'int';
    public const STRING         = 'string';
    public const LONGTEXT       = 'longtext';
    public const HTML           = 'html';
    public const BOOLEAN        = 'boolean';
    public const ARRAY_INT      = 'aint';
    public const ARRAY_STRING   = 'astring';
    public const ARRAY_BOOLEAN  = 'aboolean';

    /**
     * @var array
     */
    private $postData = null;

    /**
     * @param mixed $value
     *
     * @return int
     */
    private function parseIntValue($value) : int
    {
        return \intval($value);
    }

    /**
     * @param mixed $value
     * @param array $options
     *
     * @return string
     */
    private function parseStringValue($value, array $options) : string
    {
        $value = \str_replace(['../', './'], '', \trim($value));

        if (false === \array_key_exists('html', $options) || false === $options['html']) {
            $value = \strip_tags($value);
        }

        if (0 === \strlen($value)) {
            return $value;
        }

        if (false === \array_key_exists('maxLength', $options) || false === $options['maxLength']) {
            return $value;
        }

        return \substr($value, 0, $options['maxLength']);
    }

    /**
     * @param mixed $value
     *
     * @return bool
     */
    private function parseBooleanValue($value) : bool
    {
        return \filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * Returns the content of the $data variable with the given name
     * and parsed for the given type.
     *
     * @param string $key  name of the field
     * @param string $type any one of the self::* constants
     * @param array  $data array of values from which to parse
     *
     * @return mixed
     */
    private function retrieveValue(string $key, string $type, array $data)
    {
        if (false === \array_key_exists($key, $data)) {
            return null;
        }

        $result = null;

        switch ($type) {
            case self::INT:
                $result = $this->parseIntValue($data[$key]);

                break;
            case self::STRING:
                $result = $this->parseStringValue(
                    $data[$key],
                    [
                        'maxLength' => 200,
                    ]
                );

                break;
            case self::LONGTEXT:
                $result = $this->parseStringValue(
                    $data[$key],
                    [
                        'maxLength' => false,
                    ]
                );

                break;
            case self::HTML:
                $result = $this->parseStringValue(
                    $data[$key],
                    [
                        'html'      => true,
                        'maxLength' => false,
                    ]
                );

                break;
            case self::BOOLEAN:
                $result = $this->parseBooleanValue($data[$key]);

                break;
            case self::ARRAY_INT:
                $result = $data[$key];

                foreach ($result as $rkey => $value) {
                    $result[$rkey] = $this->parseIntValue($value);
                }

                break;
            case self::ARRAY_STRING:
                $result = $data[$key];

                foreach ($result as $rkey => $value) {
                    $result[$rkey] = $this->parseStringValue(
                        $value,
                        [
                            'maxLength' => 200,
                        ]
                    );
                }

                break;
            case self::ARRAY_BOOLEAN:
                $result = $data[$key];

                foreach ($result as $rkey => $value) {
                    $result[$rkey] = $this->parseBooleanValue($value);
                }

                break;
            default:
                $result = null;
        }

        return $result;
    }

    /**
     * Returns the content of the POST variable with the given name
     * and parsed for the given type.
     *
     * @param string $key name of the field
     * @param string $type any one of the self::* constants
     *
     * @return mixed
     */
    public function post(string $key, string $type)
    {
        return $this->retrieveValue($key, $type, $this->postData());
    }

    /**
     * @return array
     */
    public function postData() : array
    {
        if (null === $this->postData) {
            if ('application/json' === $_SERVER['CONTENT_TYPE']) {
                $this->postData = \json_decode(\file_get_contents('php://input'), true);
            } else {
                $this->postData = $_POST;
            }
        }

        return $this->postData;
    }

    /**
     * Returns the content of the GET variable with the given name
     * and parsed for the given type.
     *
     * @param string $key name of the field
     * @param string $type any one of the self::* constants
     *
     * @return mixed
     */
    public function get(string $key, string $type)
    {
        return $this->retrieveValue($key, $type, $_GET);
    }

    /**
     * Indicates if the current HTTP request is a GET request.
     *
     * @return bool
     */
    public function isGetRequest() : bool
    {
        return 'GET' === $_SERVER['REQUEST_METHOD'];
    }

    /**
     * Indicates if the current HTTP request is a POST request.
     *
     * @return bool
     */
    public function isPostRequest() : bool
    {
        return 'POST' === $_SERVER['REQUEST_METHOD'];
    }
}
