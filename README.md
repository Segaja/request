# Segaja/Request

## installation
```
composer require segaja/request
```

## Usage

```php
<?php
$request = new \Segaja\Request();

$getParameter  = $request->get('action', \Segaja\Request::STRING);

if (true === $request->isPostRequest()) {
    $postParameter = $request->post('id', \Segaja\Request::INT);
}
```

## Types
Normal `STRING` values are cut at 200 letters. If you need to retrieve a value that has more letters, you have to use `LONGTEXT`.

In `STRING` and `LONGSTRING` any html tags will be stripped, if you need to retrieve a value with html tags in takt, please use `HTML` as type.

For the types `ARRAY_INT`, `ARRAY_STRING` and `ARRAY_BOOLEAN` values the validation/sanitazation of the datatype are done for each entry in the array.

## POST data
If the content type of the request is `application/json`, then the library reads `php://input` as post data. Otherwise the normal `$_POST` is used.
